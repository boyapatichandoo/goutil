package goutil

import "sort"

//StringSort ...
func StringSort(data interface{}, less func(i int, j int) bool) interface{} {
	sort.Slice(data, less)
	return data
}
